package guiatp2_podettinh;
import java.util.Scanner;
public class EJ_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Introduce una palabra para saber si es Palindromo o no: ");
        String palabra = scanner.nextLine();
        System.out.println("La palabra " + palabra + " es palindromo?: " + esPalindromo(palabra));
    }
    
    public static boolean esPalindromo (String palabra){
        palabra = palabra.toLowerCase();
        
        for (int i = 0, j = palabra.length() - 1; i <=j; i++, j--) {
            if (palabra.charAt(i)!= palabra.charAt(j)){
                return false;
            }
        }
        return true;
    }
    
}
