package guiatp2_podettinh;
public class EJ_6 {
    public static void main(String[] args) {
        int limite = 200;
        
        System.out.println("Listado de numeros primos menores que " + limite + ":");
        
        for (int numero = 2; numero < limite; numero++) {
            if (esPrimo(numero)) {
                System.out.print(numero + ", ");
            }
        }
    }
    
    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }
        
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        
        return true;
    }
}
    

