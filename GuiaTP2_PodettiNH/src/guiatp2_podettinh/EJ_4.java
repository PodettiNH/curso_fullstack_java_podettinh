package guiatp2_podettinh;
import java.util.Scanner;
public class EJ_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa un número entero: ");
        int numero = scanner.nextInt();

        int factorial = 1;

        if (numero < 0) {
            System.out.println("El factorial no está definido para números negativos.");
        } else if (numero == 0) {
            System.out.println("El factorial de 0 es 1.");
        } else {
            for (int i = 1; i <= numero; i++) {
                factorial *= i;
            }

            System.out.println("El factorial de " + numero + " es " + factorial);
        }
    }
    
}
