/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package guiatp2_podettinh;

/**
 *
 * @author nirud
 */
public class test {

    public static void main(String[] args) {
        int[] array = {5, 2, 8, 1, 9};

        System.out.println("Array original:");
        imprimirArray(array);

        ordenarAscendente(array);

        System.out.println("Array ordenado de manera ascendente:");
        imprimirArray(array);
    }

    public static void ordenarAscendente(int[] array) {
        int n = array.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    public static void imprimirArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
}

