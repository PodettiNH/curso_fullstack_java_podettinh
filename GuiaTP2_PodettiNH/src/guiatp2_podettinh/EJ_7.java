package guiatp2_podettinh;
import java.util.Scanner;
public class EJ_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Responde las siguientes preguntas con SI o NO.");
        
        System.out.println("¿El animal es herbivoro?");
        boolean esHerbivoro = obtenerRespuesta(scanner);
        
        System.out.println("¿El animal es mamifero?");
        boolean esMamifero = obtenerRespuesta(scanner);
        
        System.out.println("¿El animal es domestico?");
        boolean esDomestico = obtenerRespuesta(scanner);
        
        String animalElegido = determinarAnimal(esHerbivoro, esMamifero, esDomestico);
        
        System.out.println("El animal elegido es: " + animalElegido);
    }
    
    private static boolean obtenerRespuesta(Scanner scanner) {
        String respuesta = scanner.nextLine().toLowerCase();
        
        return respuesta.equals("si");
    }
    
    private static String determinarAnimal(boolean esHerbivoro, boolean esMamifero, boolean esDomestico) {
        if (esHerbivoro) {
            if (esMamifero) {
                if (esDomestico) {
                    return "Caballo";
                } else {
                    return "Alce";
                }
            } else {
                if (esDomestico){
                    return "Tortuga";
                }else{
                    return "Caracol";
                }
            }
        } else {
            if (esMamifero) {
                if (esDomestico) {
                    return "Gato";
                } else {
                    return "Leon";
                }
            } else {
                if (esDomestico) {
                    return "Piton";
                } else {
                    return "Condor";
                }
            }
                
        }
    }
    }
