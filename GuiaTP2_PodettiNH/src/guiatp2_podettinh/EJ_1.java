package guiatp2_podettinh;
import java.util.*;
public class EJ_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;        
        System.out.println("Introduce un numero entero para ver su tabla de multiplicar: ");
        n = scanner.nextInt();
        System.out.println("La Tabla del " + n);
        for (int i = 1; i<=10; i++){
            System.out.println(n +" * "+ i +" = " + n*i );
        }
    }
    
}
