package guiatp1_podettinh;
import java.util.Scanner;
public class EJ_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        int edad1, edad2, AUX;
        System.out.println("Introduzca la edad del 1ro: ");
            edad1 = scanner.nextInt();
        System.out.println("Introduzca la edad del 2do: ");
            edad2 = scanner.nextInt();
        System.out.println("Los valores iniciales son 1ro: " + edad1 + " 2do: " + edad2);
        AUX = edad1;
        edad1 = edad2;
        edad2 = AUX;
        System.out.println("Los valores intercambiados son 1ro: " +  edad1 + " 2do: " + edad2);
    }   
    
}
