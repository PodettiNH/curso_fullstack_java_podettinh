package guiatp1_podettinh;
import java.util.Scanner;
public class EJ_8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        double celsius, kelvin, fahrenheit;
        System.out.println("Ingrese la temperatura en °C para transformar a °K y °F: ");
            // la variable es double
            celsius = scanner.nextInt();
            kelvin = 273.15 + celsius;
            fahrenheit = (1.8 * celsius);
        System.out.println("La temperatura en °K es: " + kelvin);
        System.out.println("La temperatura en °F es: " + fahrenheit);
         
    }
    
}
