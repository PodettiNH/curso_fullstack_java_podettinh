package guiatp1_podettinh;
import java.util.Scanner;
public class EJ_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        double precio;
        double descuento;
        double vdescontado;
        System.out.println("Ingrese el precio y su descuento");
        System.out.println("Ingrese el precio: ");
            precio = scanner.nextDouble();
        System.out.println("Ingrese el valor descuento: ");
            descuento = scanner.nextDouble(); 
            vdescontado = (precio*descuento)/100;
        
        System.out.println("El importe descontado es: " + (vdescontado));
        System.out.println("El importe a pagar es: " + (precio-vdescontado));
    }
    
}
