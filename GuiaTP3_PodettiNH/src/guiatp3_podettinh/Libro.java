package guiatp3_podettinh;
    import java.util.Scanner;
public class Libro {

    private int ISBN;
    private String Titulo;
    private String Autor;
    private int num_de_pag;
    public Libro (int ISBN, String Titulo, String Author, int num_de_pag){
        this.ISBN = ISBN;
        this.Titulo = Titulo;
        this.Autor = Autor;
        this.num_de_pag  = num_de_pag;
        
    }
    public Libro(){
        ///Vacío
    }
    public void cargalibro () {
        
        Scanner scanner = new Scanner (System.in);
        System.out.println("Introduzca el Titulo del libro");
        Titulo = scanner.nextLine();
        System.out.println ("Introduzca el Autor del libro");
        Autor = scanner.nextLine();
        System.out.println ("Introduzca el ISBN");
        ISBN = scanner.nextInt();
        System.out.println ("Introduzca el numero de paginas");
        num_de_pag = scanner.nextInt();
        
    }
    public void mostrar () {
     System.out.println("El libro "+ Titulo + (" del autor ") + Autor + (" con el ISBN ") + ISBN + (" con ") + num_de_pag + (" paginas.") );
    }
}
